class CreateConfirmations < ActiveRecord::Migration
  def change
    create_table :confirmations do |t|
      t.boolean :approved
      t.integer :approver
      t.text :disapproval_justification
      t.boolean :deleted
      t.references :request
      t.references :user

      t.timestamps
    end
    add_index :confirmations, :request_id
    add_index :confirmations, :user_id
  end
end
