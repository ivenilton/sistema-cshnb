class CreateReserveOfPhysicalSpaces < ActiveRecord::Migration
  def change
    create_table :reserves_of_physical_space do |t|
      t.date :date
      t.integer :start_time
      t.integer :end_time
      t.string :event
      t.text :justification
      t.boolean :free
      t.float :price
      t.boolean :payment_confirmed
      t.boolean :deleted
      t.references :user
      t.references :space
      t.references :reservation_status

      t.timestamps
    end
    add_index :reserves_of_physical_space, :user_id
    add_index :reserves_of_physical_space, :space_id
    add_index :reserves_of_physical_space, :reservation_status_id
  end
end
