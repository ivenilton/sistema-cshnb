class CreateTravels < ActiveRecord::Migration
  def change
    create_table :travels do |t|
      t.boolean :status
      t.references :request
      t.references :vehicle
      t.boolean :deleted

      t.timestamps
    end
    add_index :travels, :request_id
    add_index :travels, :vehicle_id
  end
end
