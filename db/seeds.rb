#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts  "Aguarde..."

Institution.create(campus: 'CSHNB - CAMPUS SENADOR HELVIDIO NUNDES DE BARROS')
# Institution.create(campus: 'CMPP - CAMPUS MINISTRO PETRÔNIO PORTELA')

Sector.create(name: 'PATRIMÔNIO')
Sector.create(name: 'TRANSPORTE')
# Sector.create(name: 'SISTEMAS DE INFORMAÇÃO')
# Sector.create(name: 'BIOLOGIA')
# Sector.create(name: 'ENFERMAGEM')
# Sector.create(name: 'NUTRIÇÃO')
# Sector.create(name: 'EXEMPLO')

Role.create(name: 'DOCENTE')
Role.create(name: 'TÉCNICO ADMINISTRATIVO')

Function.create(name: 'CAF', level: 3)
Function.create(name: 'CHEFE DE SETOR', level: 2)
Function.create(name: 'DOCENTE', level: 1)
Function.create(name: 'TÉCNICO ADMINISTRATIVO', level: 1)

# Local.create(local: 'TERESINA-PI')
# Local.create(local: 'FLORIANO-PI')
# Local.create(local: 'PARNAÍBA-PI')
# Local.create(local: 'PICOS-PI')

# Daily.create(daily: 275.0)
# Daily.create(daily: 190.0)
# Daily.create(daily: 72.0)

# DailyFunctionLocal.create(daily_id: 1, function_id: 1, local_id: 1)
# DailyFunctionLocal.create(daily_id: 2, function_id: 1, local_id: 1)
# DailyFunctionLocal.create(daily_id: 3, function_id: 1, local_id: 1)
# DailyFunctionLocal.create(daily_id: 1, function_id: 1, local_id: 2)
# DailyFunctionLocal.create(daily_id: 2, function_id: 1, local_id: 2)
# DailyFunctionLocal.create(daily_id: 3, function_id: 1, local_id: 2)

UserType.create(type_name: 'Funcionário Público')
UserType.create(type_name: 'Aluno')
UserType.create(type_name: 'Externo')


LocationType.create(name: 'SALA', deleted: false)
LocationType.create(name: 'AUDITÓRIO', deleted: false)

ReservationStatus.create(name: 'APROVADA', deleted: false)
ReservationStatus.create(name: 'EM ESPERA', deleted: false)
ReservationStatus.create(name: 'CANCELADA', deleted: false)

Space.create(name: '803', capacity: 50, observations: 'POSSUI DATA SHOW', size: 20, price_local: 20, location_type_id: 1, deleted: false)
Space.create(name: '804', capacity: 50, observations: 'POSSUI DATA SHOW', size: 20, price_local: 20, location_type_id: 1, deleted: false)
Space.create(name: '805', capacity: 50, observations: 'POSSUI DATA SHOW', size: 20, price_local: 20, location_type_id: 1, deleted: false)
Space.create(name: 'Auditório novo', capacity: 400, observations: 'POSSUI DATA SHOW', size: 100, price_local: 200, location_type_id: 2, deleted: false)


Semester.create(deleted: false, name: '2013.2', start_date: '2013-10-20', end_date: '2014-03-18')

AlocationPermanent.create(deleted: false, schedule: 3, week_day: 1, space_id: 1, semester_id: 1)
AlocationPermanent.create(deleted: false, schedule: 4, week_day: 1, space_id: 1, semester_id: 1)
AlocationPermanent.create(deleted: false, schedule: 5, week_day: 1, space_id: 1, semester_id: 1)
AlocationPermanent.create(deleted: false, schedule: 6, week_day: 1, space_id: 1, semester_id: 1)

AlocationPermanent.create(deleted: false, schedule: 5, week_day: 5, space_id: 1, semester_id: 1)
AlocationPermanent.create(deleted: false, schedule: 6, week_day: 5, space_id: 1, semester_id: 1)

AlocationPermanent.create(deleted: false, schedule: 9, week_day: 3, space_id: 1, semester_id: 1)
AlocationPermanent.create(deleted: false, schedule: 10, week_day: 3, space_id: 1, semester_id: 1)
AlocationPermanent.create(deleted: false, schedule: 11, week_day: 3, space_id: 1, semester_id: 1)




Recess.create(deleted: false, begin: '2013-12-24', end: '2014-01-05', description: 'natal' )


Holiday.create(deleted: false, description: 'DIA MUNDIAL DA PAZ', day: 1, month: 1, fixed: true)



User.create(name: 'FULANO SICRÂNO', siape: '1234', email: 'fulano@email.com', phone: '(89) 9999-8888', institution_id: 1, sector_id: 1, role_id: 1, active: true, user_type_id: 1, password: '123', cpf: '123', registration: '11j23545343', deleted: false)
User.create(name: 'SICRÂNO FULANO', siape: '4321', email: 'lano@email.com', phone: '(89) 9999-8888', institution_id: 1, sector_id: 1, role_id: 1, active: true, user_type_id: 1, password: '321', cpf: '456', registration: '11j11111',deleted: false)

Driver.create(name: 'Fulano', deleted: false)
Driver.create(name: 'Lano', deleted: false)
Driver.create(name: 'Sicrano', deleted: false)

Vehicle.create(carrier_plate: 'ASD-1111', number_of_persons: 4, vehicle_type: 'S-10', deleted: false)
Vehicle.create(carrier_plate: 'ASD-2222', number_of_persons: 30, vehicle_type: 'Buzao', deleted: false)
Vehicle.create(carrier_plate: 'ASD-3333', number_of_persons: 30, vehicle_type: 'carro a jato', deleted: false)


User.create(name: 'Fernando', siape: '1234', email: 'fernando@email.com', phone: '(89) 9999-8888', institution_id: 1, sector_id: 1, role_id: 1, active: true, user_type_id: 1, password: '123', cpf: '123', registration: '11j23545343', deleted: false)
User.create(name: 'Evandro', siape: '4321', email: 'evandro@email.com', phone: '(89) 9999-8888', institution_id: 1, sector_id: 1, role_id: 1, active: true, user_type_id: 1, password: '321', cpf: '456', registration: '11j11111', deleted: false)

puts "Pronto..."

