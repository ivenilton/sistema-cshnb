Transparencia::Application.routes.draw do


  resources :monthlies
  resources :travel_expenses
  resources :travel_drivers
  resources :travels
  resources :vehicles
  resources :drivers
  resources :participants
  resources :confirmations
  resources :requests
  resources :addresses
  resources :user_types
  resources :reserve_of_physical_spaces
  resources :reservation_statuses
  resources :alocation_permanents
  resources :spaces
  resources :location_types
  resources :semesters
  resources :recesses
  resources :holidays

  root to: 'users#login'

  # resources :administrators
  get '/administrator/login' => 'administrators#login', as: :administrator_login
  get '/administrator/logout' => 'administrators#logout', as: :administrator_logout
  get '/administrator/welcome' => 'administrators#welcome', as: :administrator_welcome
  post '/administrator/authentication' => 'administrators#authentication', as: :administrator_authentication
  post '/administrator/create' => 'administrators#create', as: :administrator_create
  get '/administrator/new' => 'administrators#new', as: :administrator_new
  
  #páginas de erro
  get '/notfound' => 'portal#notfound', as: :notfound
  
  resources :users do
    resources :durations
  end

  resources :roles
  resources :sectors
  resources :institutions
  resources :functions
  resources :locals
  resources :dailies
  resources :durations
  resources :daily_function_locals

  resources :amounts do
    resources :readjustments
  end

  resources :readjustments
  resources :requests

  get 'alocation_permanents/space/:semester_id/espacos' => 'alocation_permanents#espacos', as: 'alocation_permanents_espacos'
  get 'alocation_permanents/space/:id/:semester_id' => 'alocation_permanents#reserves', as: 'alocation_permanents_space'
  get 'alocation_permanents/space/:id/:semester_id/edit' => 'alocation_permanents#editar', as: 'alocation_permanents_editar'
  post 'alocation_permanents/spaces/:space_id/:semester_id/salvar_alocacao' => 'alocation_permanents#salvar_alocacao', as: 'salvar_alocacao'


  get 'reserve_of_physical_spaces/space/all' => 'reserve_of_physical_spaces#espacos', as: 'reserve_of_physical_spaces_espacos'
  get 'reserve_of_physical_spaces/space/:id' => 'reserve_of_physical_spaces#reservas', as: 'reserve_of_physical_spaces_reservas'
  get 'reserve_of_physical_spaces/space/:id/:mes/:ano' => 'reserve_of_physical_spaces#calendario', as: 'calendario'

  get 'reserve/space/:id/new' => 'reserve_of_physical_spaces#new', as: 'reserve_of_physical_spaces_reservas_new'
  
  get '/reserve_of_physical_spaces/aprovacoes/list' => 'reserve_of_physical_spaces#aprovacoes', as: 'aprovacoes'
  get '/reserve_of_physical_spaces/approve/:id' => 'reserve_of_physical_spaces#approve', as: 'approve_reserves'
  get '/reserve_of_physical_spaces/non_approve/:id' => 'reserve_of_physical_spaces#non_approve', as: 'non_approve_reserves'
  post '/reserve_of_physical_spaces/create' => 'reserve_of_physical_spaces#create', as: 'create_reserve'
  post '/reserve_of_physical_spaces/update/:id' => 'reserve_of_physical_spaces#update', as: 'update_reserve'
  #post 'alocation_permanents/spaces/salvar_alocacao' => 'alocation_permanents#salvar_alocacao', as: 'salvar_alocacao'

  #rotas de users
  get '/user/login' => 'users#login', as: 'user_login'
  get '/user/logout' => 'users#logout', as: 'user_logout'
  post '/user/authentication' => 'users#authentication', as: 'user_authentication'
  
  #rotas de confirmations
  get '/confirmations/approve/:id' => 'confirmations#approve', as: 'approve_request'
  post '/confirmations/non_approve/:id' => 'confirmations#non_approve', as: 'non_approve_request'
  get '/confirmations/jus_non_approve/:id' => 'confirmations#non_approve_form', as: 'non_approve_form_request'

  #rota de participants para adicionar users a viagem 
  get '/participants/add/request/:id' => 'participants#index' ,as: 'add_participants'
  post '/participants/add/request/:id' => 'participants#add_participants' ,as: 'add_participants'
   
  #rota para solicitação
  get '/request/user' => 'requests#request_users', as: 'request_users'

  

  #rotas de confirmações
  get '/confirmations/:id/travel' => 'confirmations#travel', as: 'confirmation_travel'
  get '/confirmations/request/:id/travel/new' => 'travels#new', as: 'confirmation_new_travel'
  get '/confirmations/:id/travel/show' => 'confirmations#show_travel', as: 'confirmation_show_travel'

  #rotas para adicionar motoristas a viagem
  get '/travels/:id/travel_drivers' => 'travel_drivers#index', as: 'add_driver'
  post '/travels/:id/travel_drivers' => 'travel_drivers#add_drivers', as: 'add_driver'
  resources :reserve_of_physical_spaces

  get '/relatorios/lista-de-veiculos' => 'reports#imprimir_veiculos', as: 'imprimir_veiculos'
  
end