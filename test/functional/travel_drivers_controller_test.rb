require 'test_helper'

class TravelDriversControllerTest < ActionController::TestCase
  setup do
    @travel_driver = travel_drivers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:travel_drivers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create travel_driver" do
    assert_difference('TravelDriver.count') do
      post :create, travel_driver: { number_of_daily: @travel_driver.number_of_daily, value_of_daily: @travel_driver.value_of_daily }
    end

    assert_redirected_to travel_driver_path(assigns(:travel_driver))
  end

  test "should show travel_driver" do
    get :show, id: @travel_driver
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @travel_driver
    assert_response :success
  end

  test "should update travel_driver" do
    put :update, id: @travel_driver, travel_driver: { number_of_daily: @travel_driver.number_of_daily, value_of_daily: @travel_driver.value_of_daily }
    assert_redirected_to travel_driver_path(assigns(:travel_driver))
  end

  test "should destroy travel_driver" do
    assert_difference('TravelDriver.count', -1) do
      delete :destroy, id: @travel_driver
    end

    assert_redirected_to travel_drivers_path
  end
end
