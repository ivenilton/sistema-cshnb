module MonthliesHelper
	def numero_diarias(inicio,fim)
        dias = 0
        if(inicio.year == fim.year)
          dias = fim.yday - inicio.yday 
        else
          a = inicio.end_of_year.yday - inicio.yday
          b = fim.yday - fim.at_beginning_of_year.yday 
          dias = a + b
        end
        return(dias + 1)
    end

    def conta_diarias(dias)
       return((dias - 1)+0.5)
    end

    
end


