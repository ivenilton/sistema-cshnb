module AdministratorsHelper
  def authenticate_administrator(current_administrator)
    administrator = Administrator.find_by_email(current_administrator.email)

    unless administrator.nil?
      if administrator.password == Digest::MD5.hexdigest(current_administrator.password)
        session[:administrator] = administrator
      else
        return false
      end
    else
      return false
    end
  end

  def administrator_authenticated?
    session[:administrator]
  end

  def end_session
    session[:administrator] = nil
  end
  
  def permission_admin_logged
    redirect_to notfound_path unless administrator_authenticated?
  end

  def permission_admin_not_logged
    redirect_to notfound_path if administrator_authenticated?
  end
end
