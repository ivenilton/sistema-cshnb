#encoding: utf-8
module ReserveOfPhysicalSpacesHelper

  def tipo_objeto(objeto)
    return "feriado-recesso" if objeto.class.to_s == "Recess" || objeto.class.to_s == "Holiday"
    return "alocacao-permanente" if objeto.class.to_s == "AlocationPermanent"
    return "reserva-usuario" if objeto.class.to_s == "ReserveOfPhysicalSpace"
    return ""
  end

	def schedules
  	a = [
  		'06:00 - 07:00',
  		'07:00 - 08:00',
  		'08:00 - 09:00',
  		'09:00 - 10:00',
  		'10:00 - 11:00',
  		'11:00 - 12:00',
  		'12:00 - 13:00',
  		'13:00 - 14:00',
  		'14:00 - 15:00',
  		'15:00 - 16:00',
  		'16:00 - 17:00',
  		'17:00 - 18:00',
  		'18:00 - 19:00',
  		'19:00 - 20:00',
  		'20:00 - 21:00',
  		'21:00 - 22:00']
  end

  def months
    a = [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro'
    ]
  end

  def dias_reserva_fixa(mes, ano, week_day, qtdDias, inicio_semestre, fim_semestre)
     #reserves = AlocationPermanent.where(deleted: false, space_id: params[:id])
     array = Array.new

     for i in (1..qtdDias)
       x = DateTime.new(ano, mes, i)
       array << i if week_day == x.wday && x >= inicio_semestre && x <= fim_semestre

     end 

     array
  end


def mes_anterior(mes, ano)
  m = nil
  a = ano
  if mes > 1
    m = mes - 1
  else
    m = 12
    a = ano -1
  end
  return m, a
end

#mes, ano = mes_anterior(@mes, @ano)

def mes_seguinte(mes, ano)
  m = nil
  a = ano
  if mes < 12
    m = mes + 1
  else
    m = 1
    a = ano + 1
  end
  return m,a
end


def customizar_aaah!(reserve)
  if reserve == 1
    return "success"
  end

  if reserve == 3
    return "warning"
  end

  return ""
end




end
