module UsersHelper
  def authenticate_user(login_user)
    login_user.email.downcase!
    user = User.where(email: login_user.email, deleted: false, active: true).first
    p user

    unless user.nil?
      if user.password == Digest::MD5.hexdigest(login_user.password)
        session[:user] = user
      else
        return false
      end
    else
      return false
    end
  end

  def user_authenticated?
    session[:user]
  end

  def current_user
    session[:user]
  end

  def end_session_user
    session[:user] = nil
  end

  def usuario_logado
    redirect_to notfound_path if user_authenticated?
  end

  def autorizacao_chefe_setor_patrimonio
    if user_authenticated?
      redirect_to notfound_path if current_user.user_type_id != 1 || current_user.sector_id != 1 || current_user.durations.last.nil? || current_user.durations.last.function.level != 2
    else
      redirect_to notfound_path
    end
  end

  def navbar_chefe_setor_patrimonio
    if user_authenticated? && current_user.user_type_id == 1 && current_user.sector_id == 1 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 2
      return true
    else   
      return false
    end 
  end


  def autorizacao_chefe_setor_transporte
    if user_authenticated?
      redirect_to notfound_path if current_user.user_type_id != 1 || current_user.sector_id != 2 || current_user.durations.last.nil? || current_user.durations.last.function.level != 2
    else
      redirect_to notfound_path
    end
  end

  def navbar_chefe_setor_transporte
    if user_authenticated? && current_user.user_type_id == 1 && current_user.sector_id == 2 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 2
      return true
    else   
      return false
    end 
  end
end