class Recess < ActiveRecord::Base
  attr_accessible :deleted, :begin, :description, :end

  validates :begin, presence: true
  validates :end, presence: true

  validates :description, presence: true

end
