class Vehicle < ActiveRecord::Base
  has_many :travels, :dependent => :destroy	



  attr_accessible :deleted, :carrier_plate, :number_of_persons, :vehicle_type, :travels_attributes


  validates :carrier_plate, presence: true
  validates :vehicle_type, presence: true
  validates :number_of_persons, presence: true
     


end
