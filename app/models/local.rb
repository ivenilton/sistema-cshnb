class Local < ActiveRecord::Base
  has_many :daily_function_locals
  attr_accessible :local, :daily_function_locals_attributes
end
