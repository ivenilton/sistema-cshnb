#encoding: utf-8
class ReserveOfPhysicalSpace < ActiveRecord::Base
  belongs_to :user
  belongs_to :space
  belongs_to :reservation_status

  attr_accessible :date, 
                  :end_time, 
                  :event, 
                  :free, 
                  :justification, 
                  :payment_confirmed, 
                  :price, 
                  :start_time, 
                  :user_id, 
                  :space_id, 
                  :reservation_status_id,
                  :deleted

  validates :date, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true
  validates :event, presence: true
  validates :justification, presence: true
  validates :price, presence: true
  validates :coincidiu_com_recesso, acceptance: {accept: true, message: ', não é possível realizar reserva nessa data/horário'}
  validates :coincidiu_com_feriado, acceptance: {accept: true, message: ', não é possível realizar reserva nessa data/horário'}
  validates :coincidiu_com_reserva_fixa, acceptance: {accept: true, message: ', não é possível realizar reserva nessa data/horário'}
  validates :coincidiu_com_reserva, acceptance: {accept: true, message: ', não é possível realizar reserva nessa data/horário'}

  # def coincidiu_com_recesso
  # 	# ok = true
  #   return false if self.start_time.nil? || self.end_time.nil?

  #   recesses = Recess.where(deleted: false)
  #   recesses = recesses.select {|recess| self.date.between?(recess.begin, recess.end) }
  #   return false unless recesses.empty?

  #   holidays = Holiday.where(deleted: false)
  #   holidays = holidays.select {|holiday| self.date == holiday.date || (self.date.day == holiday.day && self.date.month == holiday.month)}
  #   return false unless holidays.empty?  
    
  #   semester = Semester.where(deleted: false).last
  #   fixed_reserves = AlocationPermanent.where(deleted: false)
  #   fixed_reserves = fixed_reserves.select {|fixed_reserve| self.date.wday == fixed_reserve.week_day && (fixed_reserve.schedule >= self.start_time && fixed_reserve.schedule <= self.end_time) && fixed_reserve.semester_id == semester.id}
  #   return false unless fixed_reserves.empty?

  #   reserves = ReserveOfPhysicalSpace.where(deleted: false)
  #   reserves = reserves.select {|reserve| self.date.wday == reserve.date.wday && (reserve.start_time >= self.start_time && reserve.end_time <= self.end_time) && reserve.reservation_status_id != 3}
  #   reserves = reserves.reject {|reserve| reserve.id == self.id}
  #   #reserves = reserves.select {|reserve| self.date.wday == reserve.week_day && (reserve.schedule >= self.start_time && reserve.schedule <= self.end_time) && reserve.status_id != 3}
  #   return false unless reserves.empty?

  #   return true
    
  # end

  def coincidiu_com_recesso
    recesses = Recess.where(deleted: false)
    recesses = recesses.select {|recess| self.date.between?(recess.begin, recess.end) }
    return false unless recesses.empty?
    return true
  end


  def coincidiu_com_feriado
    holidays = Holiday.where(deleted: false)
    holidays = holidays.select {|holiday| self.date == holiday.date || (self.date.day == holiday.day && self.date.month == holiday.month)}
    return false unless holidays.empty?  
    return true
  end


  def coincidiu_com_reserva_fixa
    return false if self.start_time.nil? || self.end_time.nil?
    semester = Semester.where(deleted: false).last
    fixed_reserves = AlocationPermanent.where(deleted: false, space_id: self.space_id)
    fixed_reserves = fixed_reserves.select {|fixed_reserve| self.date.wday == fixed_reserve.week_day && (fixed_reserve.schedule >= self.start_time && fixed_reserve.schedule <= self.end_time) && fixed_reserve.semester_id == semester.id}
    return false unless fixed_reserves.empty?
    return true
  end
  #comparar dia do mês e não da semana, comprar com sala...
  def coincidiu_com_reserva
    return false if self.start_time.nil? || self.end_time.nil?
    reserves = ReserveOfPhysicalSpace.where(deleted: false, space_id: self.space_id)
    reserves = reserves.select {|reserve| (self.date.day == reserve.date.day && self.date.month == reserve.date.month && self.date.year == reserve.date.year) && ((reserve.start_time >= self.start_time && reserve.start_time <= self.end_time)||(reserve.end_time >= self.start_time && reserve.end_time <= self.end_time)) && reserve.reservation_status_id ==1}
    #reserves = reserves.reject {|reserve| reserve.id == self.id}
    #reserves = reserves.select {|reserve| self.date.wday == reserve.week_day && (reserve.schedule >= self.start_time && reserve.schedule <= self.end_time) && reserve.status_id != 3}
    return false unless reserves.empty?
    return true
  end

end