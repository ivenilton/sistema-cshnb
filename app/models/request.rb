#encoding: utf-8
class Request < ActiveRecord::Base

  include MonthliesHelper
  belongs_to :user

  has_many :confirmations, :dependent => :destroy
  has_many :participants, :dependent => :destroy
  has_one :travel, :dependent => :destroy





  attr_accessible :deleted,:date, :date_of_departure, :destination, :distance, :goal, :justification, 
  				  :return_date, :user_id, :confirmations_attributes, :participants_attributes,
  				  :travel_attributes


  validates :date, presence: true 
  validates :date_of_departure , presence: true
  validates :return_date, presence: true
  validates :destination, presence: true
  validates :distance, presence: true
  validates :goal, presence: true
  validates :justification, presence: true

  validates :user_id, presence: true #excluindo os terceirizados 
   				  
  validates :numero_de_dias , acceptance: {accept: true, message: 'não permitido'}
  
  def numero_de_dias
    a = numero_diarias(self.date_of_departure, self.return_date)
    if a > 30 || a < 1
      return false
    else
      return true
    end
  end
  


end
