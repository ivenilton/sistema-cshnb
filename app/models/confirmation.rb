#encoding: utf-8
class Confirmation < ActiveRecord::Base
  belongs_to :request
  belongs_to :user

  # remover o campo 'approver', por ser desnecessario
  attr_accessible :deleted,:approved, :approver, :disapproval_justification, :user_id, :request_id
  
  

  validates :aprovacao, acceptance: {accept: true, message: 'Solicitação negada'}

  def aprovacao
  	ok = true
  	if self.approved == false
  	  if self.disapproval_justification.empty?
  	  	ok = false
  	  end
  	end

  	return ok
  end



end
