#encoding: utf-8
class TravelDriver < ActiveRecord::Base
  belongs_to :travel
  belongs_to :driver



  attr_accessible :number_of_daily, :travel_id, :driver_id, :deleted

  #validates :number_of_daily, presence: true
 



  validates :travel_id, presence: true
  validates :driver_id, presence: true 

  validates :valida_motorista, acceptance: {accept: true, message: 'motorista não disponível'}
 
  def valida_motorista
     
     requestes = Request.where(deleted: false)
     requestes = requestes.select {|request| request.travel != nil }
     requestes = requestes.reject {|request| self.travel.request_id == request.id } 

     requestes = requestes.select{|request| self.travel.request.date_of_departure.between?(request.date_of_departure, request.return_date) || self.travel.request.return_date.between?(request.date_of_departure, request.return_date)}
     
     if requestes.empty?
        return true
     else
        requestes = requestes.select{|r| self.driver_id == r.travel.travel_drivers.first.driver_id || self.driver_id == r.travel.travel_drivers.last.driver_id }    
        if requestes.empty?
            return true
        else
            return false
        end 
     end

  end
end
