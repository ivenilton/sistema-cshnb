class Sector < ActiveRecord::Base
  has_many :users, dependent: :destroy
  attr_accessible :name, :user_attributes
end
