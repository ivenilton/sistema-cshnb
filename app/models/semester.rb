class Semester < ActiveRecord::Base
  has_many :fixed_reserves, :dependent => :destroy
  attr_accessible :deleted, :end_date, :name, :start_date, :alocation_permanents_attributes

  validates :name, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  

end
