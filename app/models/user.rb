  class User < ActiveRecord::Base

  belongs_to :institution
  belongs_to :sector
  belongs_to :role
  belongs_to :user_type
  has_many :durations, :dependent => :destroy
  has_many :functions, :through => :durations
  has_many :requests, :dependent => :destroy
  has_many :confirmations, :dependent => :destroy
  has_many :participants, :dependent => :destroy
  has_many :reserve_of_physical_spaces, :dependent => :destroy
  has_many :addresses, :dependent => :destroy

  
  attr_accessible :active,
                  :email,
                  :name,
                  :password,
                  :phone,
                  :siape,
                  :institution_id,
                  :sector_id,
                  :role_id,
                  :user_type_id,
                  :durations_attributes,
                  :functions_attributes,
                  :reserve_of_physical_spaces_attributes,
                  :addresses_attributes,
                  :requests_attributes,
                  :confirmations_attributes, 
                  :participants_attributes,
                  :cpf,
                  :registration,
                  :deleted 


  before_create do |user|
    user.active = true
    user.password = Digest::MD5.hexdigest(password)
  end

  before_save do |user|
    user.email.downcase!
  end
end
