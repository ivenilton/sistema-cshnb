class RequestsController < ApplicationController

  include ApplicationHelper

  def index
    @requests = Request.where(deleted: false, user_id: session[:user].id)
  end

  def show
    @request = Request.find(params[:id])
  end

  def new
    @request = Request.new
  end

  def edit
    @request = Request.find(params[:id])
    @request.date_of_departure = format_date_br(@request.date_of_departure)
    @request.return_date = format_date_br(@request.return_date)
  end

  def create
    @request = Request.new(params[:request])
    @request.deleted = false
    @request.user_id = session[:user].id  
    @request.date = DateTime.now
    if @request.save
      redirect_to requests_path
    else
      render :new
    end
  end

  def update
    @request = Request.find(params[:id])

    if @request.update_attributes(params[:request])
      redirect_to requests_path
    else
      render :edit
    end
  end

  def destroy
    @request = Request.find(params[:id])
    @request.update_attributes(deleted: true)

    redirect_to requests_path
  end
end
