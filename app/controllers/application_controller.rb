class ApplicationController < ActionController::Base
  protect_from_forgery
  include AdministratorsHelper

  # custom 404
  # unless Rails.application.config.consider_all_requests_local
  #   rescue_from ActiveRecord::RecordNotFound,
  #               ActionController::RoutingError,
  #               ActionController::UnknownController,
  #               ActionController::UnknownAction,
  #               ActionController::MethodNotAllowed do |exception|
  #     redirect_to '/404.html'
  #   end
  # end

end