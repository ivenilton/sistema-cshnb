class ConfirmationsController < ApplicationController

  include UsersHelper

  #before_filter :autorizacao_chefe_setor_transporte, except: []

  def index
    # @confirmations = Confirmation.where(deleted: false)
    @requests = Request.where(deleted: false)
  end

  def show
    @confirmation = Confirmation.find(params[:id]) 
  end

  def new
    @confirmation = Confirmation.new
  end

  def edit
    @confirmation = Confirmation.find(params[:id])
  end

  def create
    @confirmation = Confirmation.new(params[:confirmation])
    @confirmation.deleted = false
    
      if @confirmation.save
        redirect_to confirmations_path
      else
        render :new
      end
  end

  def update
    @confirmation = Confirmation.find(params[:id])
      if @confirmation.update_attributes(params[:confirmation])
        redirect_to confirmations_path
      else
        render :edit
      end
  end

  def destroy
    @confirmation = Confirmation.find(params[:id])
    @confirmation.update_attributes(deleted: true)

    redirect_to confirmations_path
  end

  def approve
    @request = Request.find(params[:id])
    
    if @request.confirmations.count  == 0
       Confirmation.create(approved: true, request_id: @request.id, user_id: session[:user].id)
    else 
     confirmation = @request.confirmations.first
     confirmation.update_attributes(approved: true)
    end 
    
    redirect_to confirmations_path
  end

  def non_approve
    @request = Request.find(params[:id])
    
    if @request.confirmations.count  == 0
       Confirmation.create(approved: false, request_id: @request.id, user_id: session[:user].id, disapproval_justification: params[:confirmation][:disapproval_justification])
    else 
     confirmation = @request.confirmations.first
     confirmation.update_attributes(approved: false, disapproval_justification: params[:confirmation][:disapproval_justification])
    end

    redirect_to confirmations_path 
  end

  def non_approve_form
    @request = Request.find(params[:id])
    
    if @request.confirmations.count  == 0
      @confirmation = Confirmation.new
    else 
      @confirmation = @request.confirmations.first
    end 
  end

  def travel
    r =  Request.find(params[:id])

    if r.travel.nil?
      redirect_to confirmation_new_travel_path(r.id)
    else
      redirect_to r.travel
    end

  end
end
