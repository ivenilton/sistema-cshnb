class LocationTypesController < ApplicationController

  include UsersHelper
  
  before_filter :autorizacao_chefe_setor_patrimonio, except: []

  def index
    @location_types = LocationType.where(deleted: false)
  end

  def show
    @location_type = LocationType.find(params[:id])
  end

  def new
    @location_type = LocationType.new
  end

  def edit
    @location_type = LocationType.find(params[:id])
  end

  def create
    @location_type = LocationType.new(params[:location_type])
    @location_type.deleted = false

    if @location_type.save
        redirect_to location_types_path
    else
        render :new
    end    
  end

  def update
    @location_type = LocationType.find(params[:id])

    if @location_type.update_attributes(params[:location_type])
      redirect_to location_types_path
    else
      render :edit  
    end
  end

  def destroy
    @location_type = LocationType.find(params[:id])
    @location_type.update_attributes(deleted: true)

    redirect_to location_types_path
  end
end
