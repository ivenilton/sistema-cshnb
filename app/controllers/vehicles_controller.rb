class VehiclesController < ApplicationController
  # GET /vehicles
  # GET /vehicles.json
  
  include UsersHelper

  #before_filter :autorizacao_chefe_setor_transporte, except: []



  def index
    @vehicles = Vehicle.where(deleted: false)
    
    
  end

  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
    @vehicle = Vehicle.find(params[:id])

    
  end

  # GET /vehicles/new
  # GET /vehicles/new.json
  def new
    @vehicle = Vehicle.new

   
  end

  # GET /vehicles/1/edit
  def edit
    @vehicle = Vehicle.find(params[:id])
  end

  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle = Vehicle.new(params[:vehicle])
    @vehicle.deleted = false
    
      if @vehicle.save
        redirect_to vehicles_path
      
      else
        render :new
      end
   
  end

  # PUT /vehicles/1
  # PUT /vehicles/1.json
  def update
    @vehicle = Vehicle.find(params[:id])

   
      if @vehicle.update_attributes(params[:vehicle])
        redirect_to vehicles_path
      else
        render :edit
      end
    
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    @vehicle = Vehicle.find(params[:id])
    @vehicle.update_attributes(deleted: true)

   redirect_to vehicles_path
  end
end
