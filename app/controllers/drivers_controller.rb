class DriversController < ApplicationController
  # GET /drivers
  # GET /drivers.json

  include UsersHelper

  #before_filter :autorizacao_chefe_setor_transporte, except: []

  def index
    @drivers = Driver.where(deleted: false)
  end

  # GET /drivers/1
  # GET /drivers/1.json
  def show
    @driver = Driver.find(params[:id])
  end

  # GET /drivers/new
  # GET /drivers/new.json
  def new
    @driver = Driver.new

  end

  # GET /drivers/1/edit
  def edit
    @driver = Driver.find(params[:id])
  end

  # POST /drivers
  # POST /drivers.json
  def create
    @driver = Driver.new(params[:driver])
    @driver.deleted = false
   
      if @driver.save
        redirect_to drivers_path
      else
        render :new
      end
    
  end

  # PUT /drivers/1
  # PUT /drivers/1.json
  def update
    @driver = Driver.find(params[:id])

    
      if @driver.update_attributes(params[:driver])
        redirect_to drivers_path
      else
        render :edit
      end
   
  end

  # DELETE /drivers/1
  # DELETE /drivers/1.json
  def destroy
    @driver = Driver.find(params[:id])
    @driver.update_attributes(deleted: true)

    
    redirect_to drivers_path
  end
end
