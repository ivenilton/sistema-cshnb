class HolidaysController < ApplicationController
  
  include UsersHelper
  include ApplicationHelper

  before_filter :autorizacao_chefe_setor_patrimonio, except: []
  
  def index
    @holidays = Holiday.where(deleted: false)
  end

  def show
    @holiday = Holiday.find(params[:id])
  end

  def new
    @holiday = Holiday.new
  end

  def edit
    @holiday = Holiday.find(params[:id])
    @holiday.date = format_date_br(@holiday.date)
  end

  def create
    @holiday = Holiday.new(params[:holiday])
    @holiday.deleted = false
    
    if @holiday.save
      redirect_to holidays_path
    else
      render :new  
    end
  end

  def update
    @holiday = Holiday.find(params[:id])

    if @holiday.update_attributes(params[:holiday])
      redirect_to holidays_path
    else
      render :edit
    end
  end

  def destroy
    @holiday = Holiday.find(params[:id])
    @holiday.update_attributes(deleted: true)

    redirect_to holidays_path
  end
end
