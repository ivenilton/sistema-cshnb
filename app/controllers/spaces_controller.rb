class SpacesController < ApplicationController

  include UsersHelper

  before_filter :autorizacao_chefe_setor_patrimonio, except: []

  def index
    @spaces = Space.where(deleted: false)
  end

  def show
    @space = Space.find(params[:id])
  end

  def new
    @space = Space.new
  end

  def edit
    @space = Space.find(params[:id])
  end

  def create
    @space = Space.new(params[:space])
    @space.deleted = false

    if @space.save
      redirect_to spaces_path
    else
      render :new  
    end    
  end

  def update
    @space = Space.find(params[:id])

    if @space.update_attributes(params[:space])
      redirect_to spaces_path
    else
      render :edit  
    end
  end

  def destroy
    @space = Space.find(params[:id])
    @space.update_attributes(deleted: true)

    redirect_to spaces_path
  end
end
