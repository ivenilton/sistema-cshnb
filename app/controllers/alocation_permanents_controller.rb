class AlocationPermanentsController < ApplicationController
  
  include AlocationPermanentsHelper

  include UsersHelper

  before_filter :autorizacao_chefe_setor_patrimonio, except: []

  def index
    # @fixed_reserves = FixedReserve.all
    #@spaces = Space.where(deleted: false)
    @semesters = Semester.where(deleted: false)
  end

  def espacos
    @semester_id = params[:semester_id]
    @spaces = Space.where(deleted: false)
    @semester = Semester.find(@semester_id)
  end


  def reserves
    
    @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: params[:semester_id])
    @id = params[:id]
    #@semester_id = params[:semester_id]
    #@reserves = AlocationPermanent.where(deleted: false)
    @array = Array.new(16).map {|e| e = Array.new(7).map {|ee| e = '---' } }

    @reserves.each do |r|
      @array[r.schedule][r.week_day] = 'OCUPADO'
    end
    
    @espaco = Space.find(@id)
    @semester = Semester.find(params[:semester_id])

  end


  def editar
    @space = Space.find(params[:id])
    @semester = Semester.find(params[:semester_id])
    @array = Array.new(16).map {|e| e = Array.new(7).map {|ee| e = false } }

    reserves = AlocationPermanent.where(deleted: false, space_id: @space.id, semester_id: @semester.id)

    for r in reserves
      @array[r.schedule][r.week_day] = true
    end

  end



  def salvar_alocacao
    a = params[:alocations].to_a

    #a = a.reject { |h| h[0] == "semester_id" }
    a = a.map { |h| h[1] if h[1].has_key? :ok }
    a = a.reject { |h| h.nil? }
        
    array = Array.new

    a.each do |h|
      alocacoes = AlocationPermanent.where(schedule: h[:schedule], week_day: h[:weekday], space_id: params[:space_id], semester_id: params[:semester_id]).first
      # puts "ACONTECEU ALGUMA COISA AQUI #{K}"
      if alocacoes.nil?
        array << AlocationPermanent.create(deleted: false, schedule: h[:schedule], week_day: h[:weekday], space_id: params[:space_id], semester_id: params[:semester_id]).id
      else 
        alocacoes.update_attributes(deleted: false) if alocacoes.deleted == true
        array << alocacoes.id
      end
    end

    deletar = AlocationPermanent.where(semester_id: params[:semester_id], space_id: params[:space_id]).where('"alocation_permanents"."id" NOT IN (?)', array)

    deletar.each do |d|
      d.update_attributes(deleted: true)
    end

    redirect_to alocation_permanents_space_path(params[:space_id],params[:semester_id])
  end
  
  # GET /fixed_reserves/1
  # GET /fixed_reserves/1.json
  def show
    @fixed_reserf = AlocationPermanent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @fixed_reserf }
    end
  end

  # GET /fixed_reserves/new
  # GET /fixed_reserves/new.json
  def new
    @fixed_reserf = AlocationPermanent.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fixed_reserf }
    end
  end

  # GET /fixed_reserves/1/edit
  def edit
    @fixed_reserf = AlocationPermanent.find(params[:id])
  end

  # POST /fixed_reserves
  # POST /fixed_reserves.json
  def create
    @alocation_permanent = AlocationPermanent.new(params[:fixed_reserve])

    respond_to do |format|
      if @alocation_permanent.save
        format.html { redirect_to @alocation_permanent, notice: 'Fixed reserve was successfully created.' }
        format.json { render json: @alocation_permanent, status: :created, location: @alocation_permanent }
      else
        format.html { render action: "new" }
        format.json { render json: @alocation_permanent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fixed_reserves/1
  # PUT /fixed_reserves/1.json
  def update
    @alocation_permanent = AlocationPermanent.find(params[:id])

    respond_to do |format|
      if @alocation_permanent.update_attributes(params[:@alocation_permanent])
        format.html { redirect_to @alocation_permanent, notice: 'Fixed reserve was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @alocation_permanent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fixed_reserves/1
  # DELETE /fixed_reserves/1.json
  def destroy
    @alocation_permanent = AlocationPermanent.find(params[:id])
    @alocation_permanent.destroy

    respond_to do |format|
      format.html { redirect_to @alocation_permanents_url }
      format.json { head :no_content }
    end
  end
end
