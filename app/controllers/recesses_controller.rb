class RecessesController < ApplicationController
  
  include UsersHelper
  include ApplicationHelper

  before_filter :autorizacao_chefe_setor_patrimonio, except: []
  
  def index
    @recesses = Recess.where(deleted: false)
  end

  def show
    @recess = Recess.find(params[:id])
  end

  def new
    @recess = Recess.new
  end

  def edit
    @recess = Recess.find(params[:id])
    @recess.begin = format_date_br(@recess.begin)
    @recess.end = format_date_br(@recess.end)
  end

  def create
    @recess = Recess.new(params[:recess])
    @recess.deleted = false

    if @recess.save
      redirect_to recesses_path
    else
      render :new
    end
  end

  def update
    @recess = Recess.find(params[:id])

    if @recess.update_attributes(params[:recess])
      redirect_to recesses_path
    else
      render :edit
    end
  end

  def destroy
    @recess = Recess.find(params[:id])
    @recess.update_attributes(deleted: true)
    
    redirect_to recesses_path
  end
end
