class TravelDriversController < ApplicationController
  include MonthliesHelper

  def index
    @travel_driver = TravelDriver.new(travel_id: params[:id])
    @travel_drivers = TravelDriver.where(deleted: false, travel_id: params[:id])
  end

  def show
    @travel_driver = TravelDriver.find(params[:id])
  end

  def new
    @travel_driver = TravelDriver.new
  end

  def edit
    @travel_driver = TravelDriver.find(params[:id])
  end

  def create
    @travel_driver = TravelDriver.new(params[:travel_driver])
    @travel_driver.deleted = false

    x = TravelDriver.where(travel_id: @travel_driver.travel_id, driver_id: @travel_driver.driver_id)

    if x.empty?
      if @travel_driver.save
        redirect_to add_driver_path(@travel_driver.travel_id)
      else
        redirect_to add_driver_path(@travel_driver.travel_id)
      end
    else
      x.first.update_attributes(deleted: false)
      redirect_to add_driver_path(@travel_driver.travel_id)      
    end
  end

  def update
    @travel_driver = TravelDriver.find(params[:id])

    respond_to do |format|
      if @travel_driver.update_attributes(params[:travel_driver])
        format.html { redirect_to @travel_driver, notice: 'Travel driver was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @travel_driver.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @travel_driver = TravelDriver.find(params[:id])
    @travel_driver.update_attributes(deleted: true)
    redirect_to add_driver_path(@travel_driver.travel_id)
  end

  def add_drivers
    # driver = Driver.find(params[:id])
    
    motorista_viagem = TravelDriver.new(driver_id: params[:driver][:id], travel_id: params[:id], deleted: false)
    if motorista_viagem.save
      
      mensalidade = Monthly.new(travel_id: motorista_viagem.travel_id, driver_id: motorista_viagem.driver_id, transfer: false, deleted: false, qtde: numero_diarias(motorista_viagem.request.date_of_departure, motorista_viagem.request.return_date), data: motorista_viagem.request.date_of_departure)
      
      mensalidade.save

      redirect_to add_driver_path(params[:id]) 
    else
      redirect_to add_driver_path(params[:id])
    end
  end
end

