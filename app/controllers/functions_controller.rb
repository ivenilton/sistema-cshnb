class FunctionsController < ApplicationController

  def index
    @functions = Function.all
  end

  def show
    @function = Function.find(params[:id])
  end

  def new
    @function = Function.new
  end

  def edit
    @function = Function.find(params[:id])
  end

  def create
    @function = Function.new(params[:function])

    if @function.save
      redirect_to functions_path
    else
      render :new
    end
  end

  def update
    @function = Function.find(params[:id])

    if @function.update_attributes(params[:function])
      redirect_to functions_path
    else
      render :edit
    end
  end

  def destroy
    @function = Function.find(params[:id])
    @function.destroy

    redirect_to functions_path
  end
end
