class DurationsController < ApplicationController

  def index
    @user = User.find(params[:user_id])
    @durations = @user.durations
  end

  def show
    @duration = Duration.find(params[:id])
  end

  def new
    @duration = Duration.new(user_id: params[:user_id])
  end

  def edit
    @duration = Duration.find(params[:id])
  end

  def create
    @duration = Duration.new(params[:duration])
    laster = Duration.where(user_id: @duration.user_id).last

    if @duration.save
      laster.update_attributes(:end => Time.now) unless laster.nil?
      redirect_to user_durations_path(@duration.user_id)
    else
      render :new
    end
  end

  def update
    @duration = Duration.find(params[:id])

    if @duration.update_attributes(params[:duration])
      redirect_to user_durations_path(@duration.user_id)
    else
      render :edit
    end
  end

  def destroy
    @duration = Duration.find(params[:id])
    @duration.destroy
    redirect_to user_durations_path(@duration.user_id)
  end
end
