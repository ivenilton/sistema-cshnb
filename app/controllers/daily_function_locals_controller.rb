class DailyFunctionLocalsController < ApplicationController

  def index
    @daily_function_locals = DailyFunctionLocal.all
  end

  def show
    @daily_function_local = DailyFunctionLocal.find(params[:id])
  end

  def new
    @daily_function_local = DailyFunctionLocal.new
  end

  def edit
    @daily_function_local = DailyFunctionLocal.find(params[:id])
  end

  def create
    @daily_function_local = DailyFunctionLocal.new(params[:daily_function_local])

      if @daily_function_local.save
        redirect_to daily_function_locals_path
      else
        render :new
      end
  end

  def update
    @daily_function_local = DailyFunctionLocal.find(params[:id])

    if @daily_function_local.update_attributes(params[:daily_function_local])
      redirect_to daily_function_locals_path
    else
      render :edit
    end
  end

  def destroy
    @daily_function_local = DailyFunctionLocal.find(params[:id])
    @daily_function_local.destroy

    redirect_to daily_function_locals_path
  end
end
