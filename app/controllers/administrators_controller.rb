class AdministratorsController < ApplicationController

  before_filter :permission_admin_not_logged, only: [:login, :new, :create, :recovery_password, :authentication]
  before_filter :permission_admin_logged, only: [:welcome, :logout, :update]

  def login
    if Administrator.count > 0
      @administrator = Administrator.new
    else
      redirect_to administrator_new_path
    end
  end

  def logout
    end_session
    redirect_to administrator_login_path
  end

  def welcome
  end

  def authentication
    @administrator = Administrator.new(params[:administrator])
    if authenticate_administrator(@administrator)
      redirect_to administrator_welcome_path
    else
      flash[:error] = :authentication_error
      render :login
    end
  end

  def new
    @administrator = Administrator.new
  end

  def create
    if Administrator.count > 0
      redirect_to notfound_path
    else
      @administrator = Administrator.new(params[:administrator])
      if @administrator.save
        flash[:success] = t 'messages.save_success'
        redirect_to administrator_login_path
      else
        render :new
      end
    end
  end
end
