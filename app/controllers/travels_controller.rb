#encoding: utf-8
class TravelsController < ApplicationController

  def index
    @travels = Travel.where(deleted: false)
  end

  def show
    @travel = Travel.find(params[:id])
  end

  def new
    @travel = Travel.new(request_id: params[:id])
  end

  def edit
    @travel = Travel.find(params[:id])
  end

  def create
    @travel = Travel.new(params[:travel])

    if @travel.save
      redirect_to @travel
    else
      render :new
    end
  end

  def update
    @travel = Travel.find(params[:id])

    if @travel.update_attributes(params[:travel])
      respond_to travels_path
    else
     render :edit
    end
  end

  def destroy
    @travel = Travel.find(params[:id])
    @travel.update_attributes(deleted: true)

    respond_to travels_path
  end
end
