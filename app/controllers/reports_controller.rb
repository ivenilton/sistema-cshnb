class ReportsController < ApplicationController

  def imprimir_veiculos
    @vehicles = Vehicle.all

    respond_to do |format|
      format.html
      format.pdf do
        html = render_to_string layout: false, action: "imprimir_veiculos.pdf.erb", formats: [:html,:pdf], handler: [:erb]
        kit = PDFKit.new(html)
        kit.stylesheets << "#{Rails.root}/public/bootstrap.css"
        send_data kit.to_pdf, filename: "veiculos.pdf", type: "application/pdf", disposition: "inline"
        return #to avoid double call
      end
    end
  end

end
