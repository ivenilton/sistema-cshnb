class UsersController < ApplicationController

  include UsersHelper

  before_filter :usuario_logado, only: [:login]

  def index
    @users = User.where(deleted: false)
  end

  def login
    @user = User.new
  end

  def logout
    end_session_user
    redirect_to user_login_path
  end

  def authentication
    @user = User.new(params[:user])
    if authenticate_user(@user)
      redirect_to root_path
    else
      render :login
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(params[:user])
    @user.deleted = false
    
    if @user.save
      redirect_to users_path
    else
      render :new
    end
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      redirect_to users_path
    else
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.update_attributes(deleted: true)
    redirect_to users_path
  end
end
